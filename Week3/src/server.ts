import express = require('express')
import { MetricsHandler } from './metric'
import path = require('path')
import bodyparser = require('body-parser');


const app = express();
const port: string = process.env.PORT || '8080'

app.use(express.static(path.join(__dirname, '/../public')))

app.use(bodyparser.json())
app.use(bodyparser.urlencoded())

app.set('views', __dirname + "/../views")
app.set('view engine', 'ejs');

const dbMet: MetricsHandler = new MetricsHandler('./../db/metrics')

app.post('/metrics/:id', (req: any, res: any) => {
  dbMet.saveMetric(req.params.id, req.body, (err: Error | null) => {
    if (err) throw err
    res.status(200).send()
  })
})

app.delete('/metrics/:id', (req: any, res: any) => {
  dbMet.deleteMetric(req.params.id, (err: Error | null) => {
    if (err) throw err
    res.status(200).send()
  })
})

app.get('/metrics/one/:id', (req: any, res: any) => {
  dbMet.getOneMetric(req.params.id, (err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})

app.get('/metrics/all', (req: any, res: any) => {
  dbMet.getAllMetrics((err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})

app.get('/hello/:name', (req :any, res: any) => { 
  res.render('hello.ejs', {name: req.params.name})
})

app.get('/metrics.json', (req: any, res: any) => {
  MetricsHandler.get((err: Error | null, result: any) => {
    if (err) {
      throw err
    }
    res.json(result)
  })
})

app.get('/', (req: any, res: any) => {
  res.render('homepage.ejs')
  res.end()
})

app.listen(port, (err: Error) => {
  if (err) {
    throw err
  }
  console.log(`server is listening on port ${port}`)
})

