import { LevelDB } from './leveldb'
import WriteStream from 'level-ws'

export class Metric {

  public timestamp: string
  public value: number

  constructor(ts: string, v: number) {
    this.timestamp = ts
    this.value = v
  }
}

export class MetricsHandler {
  private db: any

  constructor(dbPath: string) {
    this.db = LevelDB.open(dbPath)
  }
 
  public saveMetric(key: number, metrics: Metric[], callback: (error: Error | null) => void) {
    const stream = WriteStream(this.db)
    stream.on('error', callback)
    stream.on('close', callback)
    metrics.forEach((m: Metric) => {
      console.log("metric added :", m.timestamp, '=', m.value)
      stream.write({ key: `metric:${key}:${m.timestamp}`, value: m.value })
    })
    stream.end()
  }
  //delete a metric based on its key
  public deleteMetric(key: number, callback: (error: Error | null) => void) {
    const rs= this.db
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp : string  = data.key.split(':')[1] 
        const id : number = parseInt(temp)
        const timestamp : string = data.key.split(':')[2]
        if (id == key) {
          //delete
          console.log("key:" + id + "timestamp:" +timestamp+ ':'+ data.value)
          console.log("it is delete")
          rs.del(data.key)   
        }
      })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null)
      })

  }

  public getAllMetrics(callback: (error: Error | null, result : any) => void) {
    const result : Metric[] = []
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp : string  = data.key.split(':')[1] 
        const id : number = parseInt(temp)
        const timestamp : string = data.key.split(':')[2]
        const m :Metric = new Metric(timestamp, data.value)
        console.log(m)
        console.log(" key: " +id+ " timestamp: " +timestamp+ ':'+ data.value)
          //add to the list of metrics
          result.push(m)
        })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err, null)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null, result)
      })
  }

  public getOneMetric(key: number, callback: (error: Error | null, result: any) => void) {
    const result : Metric[] = []
    this.db.createReadStream()
      .on('data', (data: any) => {
        const temp : string  = data.key.split(':')[1] 
        const id : number = parseInt(temp)
        const timestamp : string = data.key.split(':')[2]
        //if id is same as the key
        if (id == key) {
          console.log("key : " +id+ " timestamp: " +timestamp+ ':'+ data.value)
          const metric : Metric= new Metric(timestamp, data.value)
          console.log(metric)
          result.push(metric)
        }
      })
      .on('error', (err: Error) => {
        callback(err, null)
      })
      .on('end', () => {
        callback(null, result)
      })
    }
      static get(callback: (error: Error | null, result?: Metric[]) => void) {
        const result = [
          new Metric('2013-11-04 14:00 UTC', 12),
          new Metric('2013-11-04 14:30 UTC', 15)
        ]
        callback(null, result)
      }
  }