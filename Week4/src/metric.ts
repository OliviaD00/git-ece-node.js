import { LevelDB } from './leveldb'
import WriteStream from 'level-ws'
import e from 'express'

export class Metric {

  public timestamp: string
  public value: number
  public username : string

  constructor(ts: string, v: number, u : string) {
    this.timestamp = ts
    this.value = v
    this.username = u
  }
}

export class MetricsHandler {
  public db: any

  constructor(dbPath: string) {
    this.db = LevelDB.open(dbPath)
  }
 
  public saveMetric(key: number, metrics: Metric[], callback: (error: Error | null) => void) {
    const stream = WriteStream(this.db)
    stream.on('error', callback)
    stream.on('close', callback)
    metrics.forEach((m: Metric) => {
      console.log("metric added :", m.timestamp, '=', m.value)
      stream.write({ key: `metric:${key}:${m.username}:${m.timestamp}`, value: m.value })
    })
    stream.end()
  }
  /*//delete a metric based on its key
  public deleteMetric(key: number, username : string, callback: (error: Error | null) => void) {
    const rs= this.db
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp : string  = data.key.split(':')[1] 
        const id : number = parseInt(temp)
        const timestamp : string = data.key.split(':')[3]
        const userID : string = data.key.split(':')[2]
        if ((id == key) && (userID === username)) {
          //delete
          console.log("key:" + id + "username :" +username+ "timestamp:" +timestamp+ ':'+ data.value)
          console.log("it is delete")
          rs.del(data.key)   
        }
      })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null)
      })

  }*/
  public deleteMetric(key: number, timestamp: string, username: string) {
    this.db.del(`metric:${key}:${timestamp}:${username}`)
  }
  

  public getAllMetrics(username : string, callback: (error: Error | null, result : any) => void) {
    const result : Metric[] = []
    this.db.createReadStream()
      .on('data', function (data: any) {
        const temp : string  = data.key.split(':')[1] 
        const id : number = parseInt(temp)
        const timestamp : string = data.key.split(':')[3]
        const userID : string = data.key.split(':')[2]
        const m :Metric = new Metric(timestamp, data.value, userID)
        if (userID === username)
        {
          console.log(m)
          console.log("key:" + id + "username" +username+ "timestamp:" +timestamp+ ':'+ data.value)
          //add to the list of metrics
          result.push(m)
        }
        })
      .on('error', (err: Error) => {
        console.log("error")
        callback(err, null)
      })
      .on('end', () => {
        console.log("Stream end")
        callback(null, result)
      })
  }

  public getOneMetric(key: number, username : string,  callback: (error: Error | null, result: any) => void) {
    const result : Metric[] = []
    this.db.createReadStream()
      .on('data', (data: any) => {
        const temp : string  = data.key.split(':')[1] 
        const id : number = parseInt(temp)
        const userID : string = data.key.split(':')[2]
        const timestamp : string = data.key.split(':')[3]
        //if id is same as the key
        if ((id == key)&&(userID==username)) {
          console.log("key:" + id + "username" +username+ "timestamp:" +timestamp+ ':'+ data.value)
          const metric : Metric= new Metric(timestamp, data.value, userID)
          console.log(metric)
          result.push(metric)
        }
      })
      .on('error', (err: Error) => {
        callback(err, null)
      })
      .on('end', () => {
        callback(null, result)
      })
    }
  }