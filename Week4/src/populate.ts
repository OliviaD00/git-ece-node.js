#!/usr/bin/env ts-node

import { Metric, MetricsHandler } from './metric'
import { User, UserHandler } from './user'

const met = [
  new Metric(`${new Date('2013-11-04 14:00 UTC').getTime()}`, 12, "Olivia"),
  new Metric(`${new Date('2013-11-04 14:15 UTC').getTime()}`, 10, "test"),
  new Metric(`${new Date('2013-11-04 14:30 UTC').getTime()}`, 8, "test"),
]

const users = [
  new User('Olivia', 'olivia.dalmasso@gmail.com', '1234'),
  new User('test', 'test', 'test'),
  new User('...', "...", "..."),// for test with mocha
]

const db = new MetricsHandler('./db/metrics')

db.saveMetric(0, met, (err: Error | null) => {
  if (err) throw err
  console.log('Data for metrics populated')
})

const dbUser = new UserHandler('./db/users')

users.forEach((user: User) => {
  dbUser.save(user, (err: Error | null) => {
    if (err) throw err
    console.log('Data for user populated')
  })
}) 
