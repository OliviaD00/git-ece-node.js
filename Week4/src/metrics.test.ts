import { expect } from 'chai'
import { Metric, MetricsHandler } from './metric'
import { LevelDB } from "./leveldb"

const dbPath: string = 'db_test/metrics'
var dbMet: MetricsHandler

describe('Metrics', function () {
    before(function () {
        LevelDB.clear(dbPath)
        dbMet = new MetricsHandler(dbPath)
    })

    after(function () {
        dbMet.db.close()
    })

    describe('#get metrics', function () {
        it('should get empty array on non existing group', function () {
            dbMet.getOneMetric(0, "...", function (err: Error | null, result?: Metric[]) {
                expect(err).to.be.null
                expect(result).to.not.be.undefined
                expect(result).to.be.empty
            })
        })
        it('should save and get a value empty array on no existing group', function () {
            var met: Metric[] = []
            met.push(new Metric('122431432434', 10, "..."))

            dbMet.saveMetric(0, met, (err: Error | null) => {
                dbMet.getOneMetric(0, "...", function (err: Error | null, result?: Metric[]) {
                    expect(result).to.not.be.undefined
                    if (result)
                        expect(result[0].value).to.equal(10)
                })
            })
        })
    })

    describe('#save metrics', function () {
        it('should save data and get', function () {
            var metric: Metric[] = []
            metric.push(new Metric('1384686660000', 20, "..."))
            dbMet.saveMetric(30, metric, function (err: Error | null, result?: null) {
                dbMet.getOneMetric(30, "...", function (err: Error | null, result?: Metric[]) {
                    expect(result).to.not.be.undefined
                    expect(err).to.be.null
                    if (result)
                        expect(result[0].value).to.equal(20)
                })
            })
        })
    })

    describe('#save metrics', function () {
        it('should update existing data', function () {
            var metric: Metric[] = []
            metric.push(new Metric('1384686660000', 10, "..."))
            dbMet.saveMetric(70, metric, function (err: Error | null, result?: null) {
                dbMet.getOneMetric(70, "...", function (err: Error | null, result?: Metric[]) {
                    expect(result).to.not.be.undefined
                    if (result)
                        expect(result[0].value).to.equal(10)
                    expect(err).to.be.null
                    var metric2: Metric[] = []
                    metric2.push(new Metric('1384686660000', 20, "..."))
                    dbMet.saveMetric(70, metric2, function (err: Error | null, result?: null) {
                        dbMet.getOneMetric(70, "...", function (err: Error | null, result?: Metric[]) {
                            if (result)
                                expect(result[0].value).to.equal(20)
                            expect(err).to.be.null
                        })
                    })
                })
            })
        })
    })

    describe('#delete metrics', function () {
        it('should delete data', function () {
            dbMet.deleteMetric(70,"123","...")
            dbMet.getOneMetric(70,"...", function (err: Error | null, result?: Metric[]) {
                expect(err).to.be.null
                console.log("result")
                expect(result).to.be.empty
              })
            })
        })
    })

    describe('#delete metrics', function () {
        it('should not fail if data not exist data', function () {
            dbMet.deleteMetric(0, "123", "...bis") 
            })
        })

