import express = require('express')
import { Metric, MetricsHandler } from './metric'
import path = require('path')
import bodyparser = require('body-parser');
import session = require('express-session')
import levelSession = require('level-session-store')
import { UserHandler, User } from './user'

const dbUser: UserHandler = new UserHandler('./../db/users')
const authRouter = express.Router()
const userRouter = express.Router()
const app = express();
const port: string = process.env.PORT || '8080'
const dbMet: MetricsHandler = new MetricsHandler('./../db/metrics')
const LevelStore = levelSession(session)

app.use(express.static(path.join(__dirname, '/../public')))
app.use(bodyparser.json())
app.use(bodyparser.urlencoded())
app.set('views', __dirname + "/../views")
app.set('view engine', 'ejs');

//session
app.use(session({
  secret: 'my very secret phrase',
  store: new LevelStore('./db/sessions'),
  resave: true,
  saveUninitialized: true
}))
//authorization
const authCheck = function (req: any, res: any, next: any) {
  if (req.session.loggedIn) {
    next()
  } else res.redirect('/login')
}
//authentification
authRouter.get('/login', (req: any, res: any) => {
  res.render('login')
})

authRouter.get('/signup', (req: any, res: any) => {
  res.render('signup')
})

authRouter.get('/index', authCheck, (req: any, res: any) => {
  res.render('index')
})

authRouter.get('/addMetric', authCheck, (req: any, res: any) => {
  res.render('addMetric')
})

authRouter.get('/deleteMetric', authCheck, (req: any, res: any) => {
  res.render('deleteMetric')
})

authRouter.get('/displayMetrics', authCheck, (req: any, res: any) => {
  res.render('displayMetrics')
})

authRouter.get('/signout', (req: any, res: any) => {
  delete req.session.loggedIn
  delete req.session.user
  res.redirect('/login')
})

app.use(authRouter)

app.post('/login', (req: any, res: any, next: any) => {
  dbUser.get(req.body.username, (err: Error | null, result?: User) => {
    console.log(User)
    if (err) next(err)
    if (result === undefined) {
      console.log("there will be an error")
    }
    else if (!result.validatePassword(req.body.password)) {
      res.render('errorPassword')
    } else {
      req.session.loggedIn = true
      req.session.user = result
      console.log('logged')
      res.redirect('/')
    }
  })
})

userRouter.post('/signup', (req: any, res: any, next: any) => {
  dbUser.get(req.body.username, function (err: Error | null, result?: User) {
    if (!err ) console.log("User already exists")
    else if (result !== undefined) {
      console.log("User already exists")
    } else {
      dbUser.save(req.body, function (err: Error | null) {
        if (err) {
          next(err)
        }
        console.log("it is okay. User persisted")
        res.redirect('/login')
      })
    }
  })
})

app.post('/addMetric', authCheck, (req: any, res: any, next: any) => {
  const metrics: Metric[] = []
  const metric = new Metric(req.body.timestamp, req.body.value, req.session.user.username)
  console.log(metric)
  metrics.push(metric)
  dbMet.saveMetric(req.body.id, metrics, (err: Error | null) => {
    if (err) throw err
    res.status(200).send()
  })
  res.redirect('/')
})

app.post('/deleteMetric', authCheck, (req: any, res: any, next: any) => {
  dbMet.deleteMetric(req.body.id, req.body.timestamp, req.session.user.username)
    res.redirect('/')
})

app.post('/displayMetrics', authCheck, (req: any, res: any, next: any) => {
  dbMet.getOneMetric(req.body.id, req.session.user.username, (err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
    console.log("the display metric is :" +result)
  })
})

userRouter.get('/:username', (req: any, res: any, next: any) => {
  dbUser.get(req.params.username, function (err: Error | null, result?: User) {
    if (err || result === undefined) {
      res.status(404).send("user not found. Please go back")
    } else res.status(200).json(result)
  })
})

app.get('/user/all', (req: any, res: any) => {
  dbUser.getAllUsers((err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})

app.delete('/user', authCheck, (req: any, res: any) => {
  console.log("trying to delete")
  dbUser.delete(req.session.user.username, (err: Error | null) => {
    console.log("delete")
    if (err) throw err
    res.status(200).send()
  })
})

app.use('/user', userRouter)

app.get('/', authCheck, (req: any, res: any) => {
  res.render('index')
})


app.post('/metrics/:id', (req: any, res: any) => {
  dbMet.saveMetric(req.params.id, req.body, (err: Error | null) => {
    if (err) throw err
    res.status(200).send()
  })
})

app.delete('/metrics/:id', authCheck, (req: any, res: any) => {
    dbMet.getOneMetric(req.params.id,req.session.user.username, (err: Error | null, result: any) => {
      if (err) throw err
      res.status(200).send()
      dbMet.deleteMetric(req.params.id, req.params.timestamp, req.session.user.username)
        res.status(200).send(result)
    })
})

app.get('/metrics/one/:id', (req: any, res: any) => {
  dbMet.getOneMetric(req.params.id, req.session.user.username, (err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})

app.post('/getmetrics', authCheck, (req: any, res: any) => {
  dbMet.getAllMetrics(req.session.user.username, (err: Error | null, result: any) => {
    if (err) throw err
    res.status(200).send(result)
  })
})
/*  previous version
app.get('/hello/:name', (req: any, res: any) => {
  res.render('index.ejs', { name: req.params.name })
})

app.get('/metrics.json', (req: any, res: any) => {
  MetricsHandler.get((err: Error | null, result: any) => {
    if (err) {
      throw err
    }
    res.json(result)
  })
})
*/

app.listen(port, (err: Error) => {
  if (err) {
    throw err
  }
  console.log(`server is listening on port ${port}`)
})

