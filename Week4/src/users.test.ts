import { expect } from 'chai'
import { User, UserHandler } from './user'
import { Metric, MetricsHandler } from './metric'
import { LevelDB } from "./leveldb"
import { notDeepEqual } from 'assert'

const dbPath: string = 'db_test/users'
var dbUser: UserHandler
var dbMet: MetricsHandler

describe('Users', function () {
    before(function () {
        LevelDB.clear(dbPath)
        dbUser = new UserHandler(dbPath)
    })

    after(function () {
        dbUser.db.close()
    })

    describe('#get user', function () {
        it('should get empty array on non existing group', function () {
            dbUser.get("...", function (err: Error | null, result?: User) {
                expect(err).to.be.null
                expect(result).to.not.be.undefined
                expect(result).to.be.empty
            })
        })
        it('should save and get a value empty array on no existing group', function () {
            var user: User = new User("...bis", "...bis@test.test", "test")
            dbUser.save(user, (err: Error | null) => {
                dbUser.get("...bis", function (err: Error | null, result?: User) {
                    expect(result).to.not.be.undefined
                    if (result)
                        expect(result[0].value).to.equal("...bis@test.test:test")
                })
            })
        })
    })

    describe('#save user', function () {
        it('should save data and get', function () {
            var user: User = new User("...bis", "...bis@test.test", "test")
            dbUser.save(user, function (err: Error | null) {
                dbUser.get("...bis", function (err: Error | null, result?: User) {
                    if (result)
                        expect(result[0].value).to.equal("...bis@test.test:test")
                })
            })
        })
    })

    describe('#save user', function () {
        it('should recognize user that already exits', function () {
            var user: User = new User("...bis1", "...bis@test.test", "test")
            dbUser.save(user, function (err: Error | null) {
                dbUser.get("...bis1", function (err: Error | null, result?: User) {
                    if (result) expect(err).to.be.null
                    var user2: User = new User("...bis1", "...bis2@test.test", "test2")
                    dbUser.save(user2, function (err: Error | null) {
                        dbUser.get("...bis1", function (err: Error | null, result?: User) {
                            if (result) expect(err).not.to.be.null
                        })
                    })
                })
            })
        })
    })

    describe('#delete user', function () {
        it('should delete data', function () {
            var user: User = new User("...bis1", "...bis@test.test", "test")
            dbUser.delete(user, function (err: Error | null, result?: User) {
                expect(result).to.be.undefined
                expect(err).to.be.null
            })
        })
    })

    describe('#delete user', function () {
        it('should not fail if data not exist data', function () {
            dbUser.delete("...", function (err: Error | null, result?: undefined) {
                expect(result).to.be.undefined
                expect(err).to.be.null
            })
        })
    })

    describe('#get metrics', function () {
        it('should not access metrics of another user', function () {
            dbUser.get("...", function (err: Error | null, result?: User) {
                expect(err).to.be.null
                expect(result).to.not.be.undefined
                expect(result).to.be.empty
                dbMet.getAllMetrics("test", function (err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null
                    expect(result).to.not.be.undefined
                    expect(result).to.be.empty
                })
            })
        })
    })

})