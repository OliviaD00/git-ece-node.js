import { LevelDB } from "./leveldb"
import WriteStream from 'level-ws'
import passwordHash from 'password-hash'

const passwordHash = require('password-hash')
export class User {
  public username: string
  public email: string
  private password: string = ""

  constructor(username: string, email: string, password: string) {
    this.username = username
    this.email = email
    this.password = password
    /*
          if (!passwordHashed) {
            this.setPassword(password)
          } else this.password = password
    */
  }
  static fromDb(username: string, value: any): User {
      console.log(value)
      const [password, email] = value.split(":")
      return new User(username, email, password)
  }

  public setPassword(toSet: string): void {
    this.password = toSet
  }

  public getPassword(): string {
    return this.password
  }

  public validatePassword(toValidate: String): boolean {
    console.log("To validate" + toValidate)
    console.log("Password in the db" + this.getPassword())
    return toValidate === this.getPassword()
  }

}
export class UserHandler {
  public db: any

  public get(username: string, callback: (err: Error | null, result?: User) => void) {
    this.db.get(`user:${username}`, function (err: Error, data: any) {
      console.log(data)
      console.log('get method')
      if (err) 
      {
        console.log('there is an error in the get method')
        callback(err)
      }
      if (data === undefined) {
        console.log("your data in the get method is undefined")
        callback(null, data)
      }
      else callback(null, User.fromDb(username, data))
    })
  }
  
 public getAllUsers(callback: (error: Error | null, result? : any) => void) {
  const result : User[] = []
  this.db.createReadStream()
    .on('data', function (data: any) {
      const username : string = data.key.split(':')[1]
      const password : string = data.value.split(':')[0]
      const email : string = data.value.split(':')[1]
      const u : User = new User(username, email, password)
      console.log(u)
      //add to the list of users
        result.push(u)
      })
    .on('error', (err: Error) => {
      console.log("error")
      callback(err, null)
    })
    .on('end', () => {
      console.log("Stream end")
      callback(null, result)
    })
}

  public save(Req: any, callback: (err: Error | null) => void) {
    var user = new User(Req.username, Req.email, Req.password)
    console.log(Req)
    this.db.put(`user:${user.username}`, `${user.getPassword()}:${user.email}`, (err: Error | null) => {
      callback(err)
    })
  }

  public delete(Req: any, callback: (err: Error | null) => void) {
    var user = new User(Req.username, Req.email, Req.password)
    this.db.del(`user:${user.username}`, `${user.getPassword()}:${user.email}`, function (err: Error, data: any) {
      if (err) callback(err)
      else if (data === undefined) callback(null)
      callback(null)
    })
  }

  constructor(path: string) {
    this.db = LevelDB.open(path)
  }
}