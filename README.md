# Node.JS Class Read me

## Introduction

This is my weekly work for Technologies Web - Node.js

### Week 1 : First Node & Git project
Create a basic app with three routes:

- `/` explains how `/hello` works
- `/hello` takes a `name` query parameter and
  - random names replies `hello [name]`
  - your own name replies with a short intro of yourself
  - any other replies a 404 code with a not found message

You should have an `index.js` file with the server creation and `handles.js` defining the server's callback
Add a `package.json` file with title
Add a `readme.md` file with title, introduction, run instructions and your name

Push it all to a GitLab / GitHub repositor

### Week 2 : Express JS & TypeScript
#### Part 1: ExpressJS

Using the code and repo from last module, convert everything to use `express` 
instead of doing routing and server setup manually. You should have:
  - `/` home page with description 
  - `/hello` page with the button and AJAX request for obtaining metrics

#### Part 2: TypeScript

Using the code and repo from last weeks TP and enhanced in class, 
add to `src/server.ts` the code to expose the front that we had previously
in the `index.js`. You should have:
  - `/` home page with description 
  - `/hello` page with the button and AJAX request for obtaining metrics

#### Part 3 : Storage

Add a get function to metrics module
Add a route to get a metric (one of the metric, all the metrics)
Add a delete function to metrics module
Add a route to delete a metric based on its key

#### Part 4 : Unit Testing & Middlewares

Fully implement User module: auth, CRUD & unit tests
Using the metrics module implemented on past work:
Link metrics to users 
Implement the mechanisms for a user to add metrics and retrieve them (only its own !)
On the front-end:
Display data accordingly to the connected user
Allow a user to display each of his metrics group

## Run instructions

You have to tap the command `npm install` on terminal in the repository of your project. Be aware of the installation of dependencies too (package.json)
First of all do a `npm install`
### Week 1 : First Node & Git project
git clone https://gitlab.com/OliviaD00/git-ece-node.js.git
then 
cd "git-ece-node.js/Week1"
then
Open http://localhost:8080/

For learn more about me : 
http://localhost:8080/hello?name=Olivia

For see the surprise
http://localhost:8080/hello?name=<your_name>

### Week 2 : Express JS & TypeScript

#### Express JS
To start with :
cd "git-ece-node.js/Week2/Part 1 Express JS"

then use `npm start`

open http://localhost:1338/

To have access to metrics you have the choice : 
1) Open http://localhost:1338:/hello/<your_name> and click on the button
2) Open http://localhost:1338:/metrics.json

#### TypeScript 
To start with :
cd "git-ece-node.js/Week2/Part 2 TypeScript"

then use `npm start`

open http://localhost:8080/

To have access to metrics you have the choice : 
1) Open http://localhost:8080:/hello/<your_name> and click on the button
2) Open http://localhost:8080:/metrics.json

### Week 3 : Storage

To start with :
cd "git-ece-node.js/Week3"

then use `npm start`

open http://localhost:8080/

To have access to metrics you have the choice : 
1) Open http://localhost:8080:/hello/<your_name> and click on the button
2) Open http://localhost:8080:/metrics.json

To have information about metrics open in JSON format (you can test it with postman):

1) get one metric http://localhost:8080/metrics/one/<id> (ex <id>=1)
2) get all metrics http://localhost:8080/metrics/all
3) post and save a metric : post /metrics/<id> with <timestamp<> and <value<> that you gave in the body in JSON format
4) delete a metric with a special id : delete /metrics/<id>

BE CAREFUL : the db contains some undefined elements for testing. Do not take it into account.

### Week 4 : Unit Testing & Middlewares
To start with :
cd "git_ece-node.js/git-ece-node.js/Week4"
and `npm start`

#### Unit Testing 
On the repository of the project tap (it will displays tests about users and metrics) :
npm test

Issues : 
- With recent modifications : some tests don't work yet. I work on it for the project.

#### Middlewares

- To populate (it will populate users and metrics) : npm run populate 
On postman : 
POST 
http://localhost:8080/user/all : will display all data about all users in the db
http://localhost:8080/user/<username> : will display data about a special user

- To add a metric after authentification, submit all lines in the front-end : 
http://localhost:8080/addMetric 

- To delete a metric after authentification, submit the line in the front-end : 
http://localhost:8080/deleteMetric

- To see all metrics of a user :
ON POSTMAN : http://localhost:8080/getmetrics

WITH FRONT : 
After authentification, go to : 
http://localhost:8080/displayMetrics and click to <Bring all the metrics !>

- To see metrics of a special key of the user (group of metrics) :
WITH FRONT : 
After authentification, go to 
http://localhost:8080/displayMetric and submit the key and click on the button

Issues : 
- delete (linked to the user) don't work yet
- delete user don't work
- trying to loggin with a not found user don't work (lead to an error)

## Member

Name : Olivia Dalmasso
Group : ING4 TD01