// Import a module
const http = require('http')

// Import handles.js

const handles = require('./handles')

//creation of the server
const server = http.createServer(handles.serverHandle)
server.listen(8080)