//import url and querystring
const url = require('url')
const qs = require('querystring')

//creation of server's callback
module.exports = {
  serverHandle: function (req, res) {
    const route = url.parse(req.url)
    const path = route.pathname
    const params = qs.parse(route.query)

    res.writeHead(200, { 'Content-Type': 'text/plain' });

    if (path === '/') {
      res.write("How hello works ? Please add on the web url /hello?name=the_name_of_you_choice. If you put the name Olivia, you will know more about me ! ")
    }

    else if (path === '/hello' && 'name' in params) {
      if (params['name'] === 'Olivia') {
        res.write('Hello, my name is Olivia Dalmasso. I am 21 years old. I am a student from ECE.Paris. Nice to meet you !!')
      }
      else {
        res.write('Hello ' + params['name'])
      }
    }
    else {
      //code 404
      res.writeHead(404, { 'Content-Type': 'text/plain' });
      //not found message
      res.write('Error 404 : not found :(')
    }
    res.end();
  }
}
